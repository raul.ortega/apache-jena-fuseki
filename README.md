# Apache Jena Fuseki

## Run docker image

```
docker run -p 3030:3030 stain/jena-fuseki
```
Read more about image [stain/jena-fuseki](https://hub.docker.com/r/stain/jena-fuseki).

Output should be similar to:

```
###################################
Initializing Apache Jena Fuseki

Randomly generated admin password:

admin=SqwQUNFm990su4E

###################################
[2023-06-13 10:02:09] Server     INFO  Apache Jena Fuseki 3.14.0
[2023-06-13 10:02:09] Config     INFO  FUSEKI_HOME=/jena-fuseki
[2023-06-13 10:02:09] Config     INFO  FUSEKI_BASE=/fuseki
[2023-06-13 10:02:09] Config     INFO  Shiro file: file:///fuseki/shiro.ini
[2023-06-13 10:02:09] Server     INFO  Started 2023/06/13 10:02:09 UTC on port 3030
```
##  Create repository

To add a new dataset, follow these instructions:

```
1.- Browse to http://localhost:3030/index.html
2.- Login as admin and password generated during initialization (docker output log)
3.- Click on link "Add one" (http://localhost:3030/manage.html?tab=new-dataset)
4.- Provide dataset name: "adivaDB"
5.- Select Persistent (TDB2)
6.- Click on "Create dataset"
```

## Upload data

First unzip trig files from trig_files.zip

```
unzip trig_files.zip
```

To upload the data to the new repository:

```
1.- Browse to http://localhost:3030/dataset.html
2.- Click on tab "Upload files"
3.- Click "+ select files" and select your *.trig files (drag and drop also works).
4.- Click "upload all"
```

For some files the upload might be unsuccessful due to missing xsd adn or rdf prefixes. Those prefixes can be added to the top of the file with these bash commands:

```
 sed -i '1s/^/@prefix xsd: <http:\/\/www.w3.org\/2001\/XMLSchema#> .\n/' *.trig
 sed -i '1s/^/@prefix rdf: <http:\/\/www.w3.org\/1999\/02\/22-rdf-syntax-ns#> .\n/' *.trig
```

xsd and rdf prefixes will be added on the top for all trig files.

Alternatively on query tab, you can add the missing prefixes by clicking on "+" button (Add a SPARQL prefix).

## SPARQL queries

To execute sparql queries against the dataset:

```
1.- Click on tab "dataset"
2.- Click on tab "query"
3.- Paste this query as an example:

PREFIX ONTOAVIDA: <http://purl.obolibrary.org/obo/ONTOAVIDA_>
PREFIX RO: <http://purl.obolibrary.org/obo/RO_> 
select * where { 
	?s RO:0000056 ?o .
}

4.- Click on play button
```

The results should be a table that looks like this:

```
Showing 1 to 50 of 131 entries

1	ONTOAVIDA:organism_1	ONTOAVIDA:avida_experiment_1
2	ONTOAVIDA:organism_2	ONTOAVIDA:avida_experiment_1
3	ONTOAVIDA:organism_3	ONTOAVIDA:avida_experiment_1
4	ONTOAVIDA:organism_4	ONTOAVIDA:avida_experiment_1
...
..
.
```

## REST API

### SELECT queries

Coy and save following bash code as query.sh

```
#!/bin/bash

# Endpoint de Fuseki
endpoint="http://localhost:3030/avidaDB"

# Consulta SPARQL
query="PREFIX ONTOAVIDA: <http://purl.obolibrary.org/obo/ONTOAVIDA_>
PREFIX RO: <http://purl.obolibrary.org/obo/RO_>
select * where { 
	?s RO:0000056 ?o .
}"

# Realizar la consulta SPARQL utilizando curl
curl -H "Accept: text/tab-separated-values	" -G --data-urlencode "query=$query" "$endpoint"
```

Then give executions permission and launch it:

```
# give execution permission
chmod +x query.sh

# execute
./query.sh
```

The output should be similar to:

```
s	?o
<http://purl.obolibrary.org/obo/ONTOAVIDA_organism_1>	<http://purl.obolibrary.org/obo/ONTOAVIDA_avida_experiment_1>
<http://purl.obolibrary.org/obo/ONTOAVIDA_organism_2>	<http://purl.obolibrary.org/obo/ONTOAVIDA_avida_experiment_1>
<http://purl.obolibrary.org/obo/ONTOAVIDA_organism_3>	<http://purl.obolibrary.org/obo/ONTOAVIDA_avida_experiment_1>
...
```

### INSERT queries

Coy and save following bash code as insert.sh

```
#!/bin/bash

# Endpoint de Fuseki
endpoint="http://localhost:3030/avidaDB"

# Consulta SPARQL
query="PREFIX RO: <http://purl.obolibrary.org/obo/RO_>
PREFIX ONTOAVIDA: <http://purl.obolibrary.org/obo/ONTOAVIDA_>
insert data { 
    ONTOAVIDA:organism_1 RO:0000056 ONTOAVIDA:avida_experiment_2 .
}"

# Realizar la consulta SPARQL utilizando curl
curl -X POST "$endpoint" --data-raw "update=$query"
```

Then give executions permission and launch it:

```
# give execution permission
chmod +x insert.sh

# execute
./insert.sh
```

The output should be similar to:

```
<html>
<head>
</head>
<body>
<h1>Success</h1>
<p>
Update succeeded
</p>
</body>
</html>
```

### DELETE queries

Coy and save following bash code as delete.sh

```
#!/bin/bash

# Endpoint de Fuseki
endpoint="http://localhost:3030/avidaDB"

# Consulta SPARQL
query="PREFIX RO: <http://purl.obolibrary.org/obo/RO_>
PREFIX ONTOAVIDA: <http://purl.obolibrary.org/obo/ONTOAVIDA_>
delete data { 
    ONTOAVIDA:organism_1 RO:0000056 ONTOAVIDA:avida_experiment_2 .
}"

# Realizar la consulta SPARQL utilizando curl
curl -X POST "$endpoint" --data-raw "update=$query"
```

Then give executions permission and launch it:

```
# give execution permission
chmod +x delete.sh

# execute
./delete.sh
```

The output should be similar to:

```
<html>
<head>
</head>
<body>
<h1>Success</h1>
<p>
Update succeeded
</p>
</body>
</html>
```

