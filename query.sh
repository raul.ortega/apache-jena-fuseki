#!/bin/bash

# Endpoint de Fuseki
endpoint="http://localhost:3030/avidaDB"

# Consulta SPARQL
query="PREFIX ONTOAVIDA: <http://purl.obolibrary.org/obo/ONTOAVIDA_>
PREFIX RO: <http://purl.obolibrary.org/obo/RO_>
select * where { 
	?s RO:0000056 ?o .
}"

# Realizar la consulta SPARQL utilizando curl
curl -H "Accept: text/tab-separated-values	" -G --data-urlencode "query=$query" "$endpoint"

