#!/bin/bash

# Endpoint de Fuseki
endpoint="http://localhost:3030/avidaDB"

# Consulta SPARQL
query="PREFIX RO: <http://purl.obolibrary.org/obo/RO_>
PREFIX ONTOAVIDA: <http://purl.obolibrary.org/obo/ONTOAVIDA_>
insert data { 
    ONTOAVIDA:organism_1 RO:0000056 ONTOAVIDA:avida_experiment_2 .
}"

# Realizar la consulta SPARQL utilizando curl
curl -X POST "$endpoint" --data-raw "update=$query"
